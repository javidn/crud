package az.ingress.lesson5.service;

import az.ingress.lesson5.dto.Student3Dto;
import az.ingress.lesson5.dto.StudentDto;
import az.ingress.lesson5.mapper.Student3Mapper;
import az.ingress.lesson5.model.Student3;
import az.ingress.lesson5.repository.Student3Repository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class Student3ServiceTest {
    // This class does unit testing.
    @InjectMocks // it is for the class which I want to test
            Student3Service student3Service;

    @Mock // it is for the class which are in class which I want to test and I want to mock that dependency
    Student3Repository student3Repository;

    @Mock
    Student3Mapper student3Mapper;

    private Student3 student3;
    private Student3Dto student3Dto;

    @BeforeEach // it ensures that the following method will process before any method.
    public void setUp() {
        student3 = Student3.builder()
                .id(5)
                .name("CavidTest")
                .age(42)
                .birthDay(new Timestamp(System.currentTimeMillis()))
                .isActive(true)
                .status("Active")
                .build();
        student3Dto = Student3Dto.builder()
                .id(5)
                .status("Online")
                .isActive(true)
                .birthDay(new Timestamp(System.currentTimeMillis()))
                .status("Active")
                .age(42)
                .newAge(42)
                .build();
    }

    @Test
    void givenValidIdWhenGetStudent3ThenSuccess() {
        // Arrange
        when(student3Repository.findById(anyInt())).thenReturn(Optional.of(student3)); // it ensures that student3 will be returned when findById(any integer number) is called.
        when(student3Mapper.studentToStudentDto(any())).thenReturn(student3Dto);
        //Act
        Student3Dto result = student3Service.get(1);
        //Assert
        assertThat(result.getId()).isEqualTo(student3.getId());
        assertThat(result.getIsActive()).isEqualTo(student3.getIsActive());
        verify(student3Repository, times(1)).findById(anyInt()); // It tests that if findById method of st3Repo was called 1 time
        verify(student3Repository, times(0)).save(any());


    }

    @Test
    void givenInvalidIdWhenGetStudent3ThenException() {
        when(student3Repository.findById(anyInt())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> student3Service.get(1)).isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Student3 with id 1 not found");
        verify(student3Mapper, times(0)).studentToStudentDto(any());
    }

    @Test
        // The test is for the case which does not have @Transactional annotation.
        // If You want to test the case which has annotation, so it is enough to test If result and your first
        // object is equal. Because case with transaction saves only one time while the transaction does "roll back".
        // It means that you don't need to capture the save method.
    void givenValidIdWhenUpdateNameThenSuccess() {
        //act
        Student3 newStudent = new Student3(student3);
        when(student3Repository.findById(anyInt())).thenReturn(Optional.of(newStudent));
        when(student3Repository.save(any())).thenReturn(newStudent);

        //arrange
        Student3 result = student3Service.updateName(5);


        //assert
        ArgumentCaptor<Student3> captor = ArgumentCaptor.forClass(Student3.class);
        verify(student3Repository, times(1)).save(captor.capture());
        Student3 captorStudent = captor.getValue();

        // Following tests if saved object(captorStudent) is equal to my first object(student3)
        assertThat(captorStudent.getName()).isEqualTo("NewName");
        assertThat(captorStudent.getId()).isEqualTo(student3.getId());
        assertThat(captorStudent.getAge()).isEqualTo(student3.getAge());
        assertThat(captorStudent.getStatus()).isEqualTo(student3.getStatus());
        assertThat(captorStudent.getBirthDay()).isEqualTo(student3.getBirthDay());
        assertThat(captorStudent.getIsActive()).isEqualTo(student3.getIsActive());

        // Following tests if the object(result) returned from updateName method is equal to my first object(student3)
        assertThat(result.getName()).isEqualTo("NewName");
        assertThat(result.getId()).isEqualTo(student3.getId());
        assertThat(result.getAge()).isEqualTo(student3.getAge());
        assertThat(result.getStatus()).isEqualTo(student3.getStatus());
        assertThat(result.getBirthDay()).isEqualTo(student3.getBirthDay());
        assertThat(result.getIsActive()).isEqualTo(student3.getIsActive());
    }

}