package az.ingress.lesson5.model;

import net.bytebuddy.asm.MemberSubstitution;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
//import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CalculatorTest {

    @InjectMocks
    private Calculator calculator;

    @Test
    void testingAdditionSuccess() {
        int result = calculator.add(1, 2);
        assertThat(result).isEqualTo(3);

    }

    @ParameterizedTest
    @CsvSource({
            "1, 2, 3",
            "2, 4, 6",
            "14, 45, 59"
    })
    void testingAdditionSuccessWithParameter(int a, int b, int expectedResult) {
        int actualResult = calculator.add(a, b);
        assertThat(actualResult).isEqualTo(expectedResult);

    }

    @ParameterizedTest
    @MethodSource("valuesForAdditionSuccess")
    void testingAdditionSuccessWithParameter2(int a, int b, int expectedResult) {
        int actualResult = calculator.add(a, b);
        assertThat(actualResult).isEqualTo(expectedResult);

    }

    private static Stream<Arguments> valuesForAdditionSuccess() {
        return Stream.of(
                Arguments.of("1", "2", "3"),
                Arguments.of("2", "4", "6"),
                Arguments.of("3", "5", "8")
        );
    }



    @Test
    void testingDivisionSuccess() {
        double result = calculator.divide(2, 1);
        assertThat(result).isEqualTo(2);
    }

    @Test
    void testingDivisionExceptionByZero() {
        // arrange
        // act and assert are together below
        assertThatThrownBy(() -> calculator.divide(1, 0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Divide by zero");
    }

}