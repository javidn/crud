package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AuthotrityRepository extends JpaRepository<Authority, Integer> {
}
