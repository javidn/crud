package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Account;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Override
//    @Lock(LockModeType.PESSIMISTIC_READ) // it leads, if a transaction gets one entity another can not update or delete, that transaction can only select
//    @Lock(LockModeType.PESSIMISTIC_WRITE)
//    @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
    @Lock(LockModeType.OPTIMISTIC)
//    @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
    Optional<Account> findById(Integer id);
}