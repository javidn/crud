package az.ingress.lesson5.repository;

import az.ingress.lesson5.dto.StudentDto;
import az.ingress.lesson5.model.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface StudentRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {

    Student findByName(String name);

    List<Student> findAllByAgeGreaterThan(Integer age);

    List<Student> findAllByAgeLessThan(Integer age);

    List<Student> findAllByAgeAndNameLike(Integer age, String name);

    //variant 1
    @Query(value = "select s.id as id, s.name as name from Student s where s.age = :age", nativeQuery = false)
    List<StudentProjection> findallByAge(@Param("age") Integer age);

    //variant 2
    @Query("select new az.ingress.lesson5.dto.StudentDto(s.id, s.name) from Student s where s.age = :age")
    List<StudentDto> findallByAgeDto(@Param("age") Integer age);


    @Query(value = "select s from Student s where s.age = :age", nativeQuery = false)
    List<Student> findallByAge3(@Param("age") Integer age);

    @Query(nativeQuery = true, value = "select s.* from authority a join student_authority sa " +
            "on a.id = sa.authority_id " +
            "join student s on sa.student_id = s.id " +
            "where a.id = :authorityId")
    Set<Student> findAllByAuthorityId(Integer authorityId);


    @Override
//    @Query(nativeQuery = false, value = "select s from Student as s join fetch s.authorities")
//    @EntityGraph(attributePaths = ("authorities"))
    // yuxaridaki iki annotasiyanin her hansi biri deyir ki authorities fieldini join use ederek doldur
    @EntityGraph(value = "Student.authorities", type = EntityGraph.EntityGraphType.LOAD)
//    load yeni ki entityde isare olunan fieldleri eager kimi et yeni join et, yerde qalanlari
//    ise default necedise ele et.
//    fetch ise deyir ki isare olunanlari eager yerde qalanlari ise default ne olduguna
//    baxmadan lazy et
    List<Student> findAll();

}
