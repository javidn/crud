package az.ingress.lesson5.repository;

public interface StudentProjection {

    String getName();
    Integer getId();
}
