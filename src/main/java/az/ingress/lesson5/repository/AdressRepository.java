package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Adress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdressRepository extends JpaRepository<Adress, Integer> {
}
