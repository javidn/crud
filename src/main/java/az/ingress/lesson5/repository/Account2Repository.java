package az.ingress.lesson5.repository;


import az.ingress.lesson5.model.Account2;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface Account2Repository extends CrudRepository<Account2, Long> {
}
