package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Student3;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Student3Repository extends JpaRepository<Student3, Integer> {
}
