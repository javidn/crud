package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Integer> {
}
