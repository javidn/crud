package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Phone;
import az.ingress.lesson5.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone, Integer> {

    int deleteAllByStudent(Student student);
}
