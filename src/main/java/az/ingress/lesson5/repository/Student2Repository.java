package az.ingress.lesson5.repository;

import az.ingress.lesson5.model.Student2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Student2Repository extends JpaRepository<Student2, Integer> {

    Student2 findByName(String name);
}