package az.ingress.lesson5;


import az.ingress.lesson5.config.ConfigurationProp;
import az.ingress.lesson5.model.*;
import az.ingress.lesson5.repository.*;
import az.ingress.lesson5.service.StudentService;
import az.ingress.lesson5.service.TransferService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
@EnableCaching
//@Transactional
public class Lesson5Application implements CommandLineRunner {

//	@Value("${ms24.lesson}")
//	private Integer lessonNumber;

    @Value("${asan.url}")
    private String asanURL;
    private final AccountRepository accountRepository;
    private final ConfigurationProp configurationProp;
    private final EntityManagerFactory entityManagerFactory;
    private final StudentService studentService;
    private final StudentRepository studentRepository;
    private final Student2Repository student2Repository;
    private final AdressRepository adressRepository;
    private final PhoneRepository phoneRepository;
    private final AuthotrityRepository authotrityRepository;
    private final TransferService transferService;


    public static void main(String[] args) {
        SpringApplication.run(Lesson5Application.class, args);
//        System.out.println("I am here.");
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void run(String... args) throws InterruptedException {
        System.out.println("\n".repeat(10));
        //Lesson24


    }


    private void lesson17_1_and_2(){
        Student student = studentService.getStudent(95);
        System.out.println(student.getAge());

        Student2 std2 = student2Repository.findByName("Cavid"); //----------
        System.out.println(std2);
        Student2 student3 = student2Repository.findById(150).get(); // -----------
        System.out.println(student3);
    }

    private void lesson16() throws InterruptedException{
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Account account1 = new Account(); // as New
        {
            EntityManager entityManager1 = entityManagerFactory.createEntityManager();
            entityManager1.getTransaction().begin();
            Account account = entityManager1.createQuery("select a from Account as a where id = 1", Account.class)
                    .getSingleResult();
            account.setBalance(1000.);

            entityManager1.getTransaction().commit(); // as managed
            entityManager1.close();

            Account account2 = accountRepository.findById(1).get();
            account2.setBalance(2000.); // as managed

        }

        {
            Account account = Account.builder()
                    .balance(150.)
                    .build();
            accountRepository.save(account);
            double d = 100;
            double d1 = 0;
            d+=100;
            account.setBalance(250. + (d1+=(d = d + 100)));
            account.setBalance(250. + (d1+=(d = d + 100)));
            System.out.println("Account balance: " + account.getBalance());
        }


        {
            entityManager.getTransaction().begin();
            Account account = Account.builder()
                    .balance(150.)
                    .build();
            entityManager.persist(account);
            entityManager.detach(account);
            account.setBalance(250.);
            entityManager.getTransaction().commit();
            entityManager.close();
        }

        {
            entityManager.getTransaction().begin();
            Account account = Account.builder()
                    .balance(1010.)
                    .build();
            entityManager.persist(account);
            account.setBalance(1020.);
            entityManager.detach(account);
            account.setBalance(200.);
            Thread.sleep(6000);
            entityManager.merge(account);
            entityManager.getTransaction().commit();

            entityManager.close();
        }

        {
            Account account = Account.builder()
                    .balance(1.)
                    .build();
            Account account2 = Account.builder()
                    .balance(2.)
                    .build();
            entityManager.getTransaction().begin();
            entityManager.persist(account);
            entityManager.persist(account2);
            account.setBalance(10.);
//            entityManager.flush();
            account2.setBalance(20.);
            entityManager.merge(account);
            entityManager.merge(account2);

            entityManager.detach(account);
            entityManager.detach(account2);
            entityManager.getTransaction().commit();
            entityManager.close();
        }

        entityManager.clear(); // I do not what it does.
    }

    private void lesson15_2() throws Exception {
        System.out.println("Befero transfer: ");
        System.out.println(accountRepository.findById(1).get().getBalance());
        System.out.println(accountRepository.findById(2).get().getBalance());
        transferService.transfer2(1, 2, 50);
//        transferService.transferSafe(1,2,50);
        System.out.println("After transfer: ");
        System.out.println(accountRepository.findById(1).get().getBalance());
        System.out.println(accountRepository.findById(2).get().getBalance());
    }

    private void lesson15_1() {
//        CriteriaBuilder cb = entityManagerFactory.getCriteriaBuilder();
//        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
//        Root<Student> root1 = cq.from(Student.class);//bu 3 setri arasdirmadim zaten muellim de demedi. Bos ver mence.
//
//        Specification<Student> specification =
//                (root, query, creiteriaBuilder) -> creiteriaBuilder.equal(root.get("isActive"), true);
//
////        specification.toPredicate(root1, cq, cb);
//
//        Specification<Student> specification1 =
//                (root, query, creiteriaBuilder) -> creiteriaBuilder.equal(root.get("age"), 45);
//
//        Specification<Student> specification2 = (root, query, creiteriaBuilder) -> {
//            Join<Student, Authority> join = root.join("authorities", JoinType.INNER);
//            return creiteriaBuilder.equal(join.get("role"), Role.USER);
//        };
//
//        List<Student> all = studentRepository.findAll(specification.and(specification1).and(specification2));
//        System.out.println(all);
//
//        Predicate predicate = cb.lessThan(root1.get("age"), 45);
    }


    //lesson 13
    private void createAuthority() {
        Authority authority = Authority.builder()
                .role(Role.USER)
                .build();
        Authority authority1 = Authority.builder()
                .role(Role.ADMIN)
                .build();
        Authority authority2 = Authority.builder()
                .role(Role.THIRD)
                .build();
        Iterable<Authority> authorities = Arrays.asList(authority, authority1, authority2);
        authotrityRepository.saveAll(authorities);
    }

    private void create13(String phoneNumber) {
        Adress adress = Adress.builder()
                .city("Berlin")
                .country("Ger")
                .street("Thomes.M")
                .build();
        Phone phone = Phone.builder()
                .number(phoneNumber)
                .build();
        Student student = Student.builder()
                .name("Fariz")
                .age(22)
                .status("Offline")
                .adress(adress)
                .phones(new HashSet<>())
                .isActive(true)
                .birthDay(new Timestamp(System.currentTimeMillis()))
                .authorities(new HashSet<>())
                .build();
        student.getPhones().add(phone);
        student.getAuthorities().add(authotrityRepository.findById(1).get());
        student.getAuthorities().add(authotrityRepository.findById(3).get());
        studentRepository.save(student);
        phone.setStudent(student);
    }


    //lesson before 12
    private void lessonsBefore12() throws Exception {

        Optional<Student> byId = studentRepository.findById(22);
        System.out.println(byId);
        if (byId.isPresent())
            System.out.println(byId.get());
        else System.out.println("Id is not found");

        {
            log.info("ms24 Lesson number is {}", configurationProp.getLesson());
            System.out.println();
            log.info("ms24 Lesson users is {}", configurationProp.getUsers());
            System.out.println();
            log.info("ms24 Lesson object is {}", configurationProp.getObjects());
            System.out.println();
            log.info("ms24 Lesson map is {}", configurationProp.getMap());
            System.out.println();
            log.info("ms24 Lesson asanURL is {}", asanURL);
            System.out.println("\n\n\n");
        }

        // jdbc
        jdbcConnectionDetails:
        {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5431/db",
                    "postgres", "password");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from book");
            while (resultSet.next()) {
                System.out.println("id = " + resultSet.getInt(1) +
                        ", name = " + resultSet.getString(2) +
                        ", author = " + resultSet.getString(3) + ", pages = " + resultSet.getInt(4));
            }
        }
        //JPA
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // sql

        List<Student> resultList = entityManager.createNativeQuery("select * from student where age > 21", Student.class)
                .getResultList();
        List<Student> resultList1 = entityManager.createQuery("select s.name from Student s where age > 21", Student.class)
                .getResultList();// yarim olanda islemir


        System.out.println(resultList);

        List<Student> resultList2 = entityManager.createNativeQuery("select * from student where age >= :age", Student.class)
                .setParameter("age", 23)
                .getResultList();
        List<Student> resultList3 = entityManager.createNativeQuery("select * from student", Student.class)
                .setFirstResult(2)
                .setMaxResults(2)
                .getResultList();

        for (Student student : resultList1) {
            System.out.println(" - " + student);
        }
        System.out.println("---------------");
        for (Student student : resultList2) {
            System.out.println(student);
        }
        for (Student student : resultList3) {
            System.out.println(student);
        }

        entityManager.getTransaction().begin();

        int executeUpdate = entityManager.createNativeQuery("update student set status = :status where isActive = :isActive", Student.class)
                .setParameter("status", "Online")
                .setParameter("isActive", true)
                .executeUpdate();

        System.out.println(executeUpdate + " rows updated");
        entityManager.getTransaction().begin();

        entityManager.getTransaction().commit();
        entityManager.getTransaction().rollback();
        entityManager.close();

        System.out.println();
        //Hibernate Query Language
        List<Student> students = entityManager.createQuery("select s.name from Student s", Student.class)
                .getResultList();

        for (Student student : students) {
            System.out.println(student);
        }
        entityManager.close();

        //Hibernate - ORM
        Session session = entityManagerFactory.createEntityManager().unwrap(Session.class);
        Student student1 = session.get(Student.class, 1);
        System.out.println(student1);
        List<Student> result = session.createNativeQuery("select id, name from student where age > 22", Student.class)
                .getResultList();// natamam olanda islemir


        System.out.println(result);
        for (Student student : result) {
            System.out.println(student);
        }
        session.getTransaction().begin();
        session.getTransaction().commit();
        session.getTransaction().rollback();
        session.close();

        Student studentById = studentService.getStudentById(2);
        System.out.println(studentById);

    }

    //lesson 12
    private int deletePhoneInStudent(Integer id, String phoneNumber) {
        AtomicInteger atomicInteger = new AtomicInteger(-1);
        Student student = studentRepository.findById(id).get();
        student.getPhones().stream().filter(phone -> phone.getNumber().equals(phoneNumber)).findFirst()
                .ifPresent(phone -> {
                    student.getPhones().remove(phone);
                    atomicInteger.set(phone.getId());
                    phoneRepository.delete(phone);
                    studentRepository.save(student);
                });

        return atomicInteger.get();
    }

    private void deletePhoneInStudent2(Integer studentId, String phoneNumber) {
        Student myStudent = getMyStudent(studentId);
        boolean b = myStudent.getPhones().removeIf(phone -> phone.getNumber().equals(phoneNumber));
        System.out.println(b);
        studentRepository.save(myStudent);
    }

    private void deleteAllPhoneInStudent(Integer studentId) {
        phoneRepository.deleteAllByStudent(getMyStudent(studentId));
    }

    private void create(String phoneNumber) {
        Adress adress = Adress.builder()
                .city("Naxcivan")
                .country("Az")
                .street("A.Enistein 12")
                .build();
        Phone phone = Phone.builder()
                .number(phoneNumber)
                .build();
        Student student = Student.builder()
                .name("Rustem")
                .age(40)
                .status("Online")
                .adress(adress)
                .isActive(true)
                .birthDay(new Timestamp(System.currentTimeMillis()))
                .phones(new HashSet<>())
                .build();
        student.getPhones().add(phone);
//        phoneRepository.save(phone);
        studentRepository.save(student);
        phone.setStudent(student);

    }

    private Student getMyStudent(Integer studentId) {
        Student student = studentRepository.findById(studentId).get();
        return student;
    }

    private void addPhoneInsideStudent(Integer studentId, String phoneNumber) {
        Student student = getMyStudent(studentId);
        student.getPhones().add(Phone.builder()
                .number(phoneNumber)
                .student(student)
                .build());
    }

    private void deletePhoneInStudent3(Integer studentId, String phoneNumber) {
        Student student = getMyStudent(studentId);
        Set<Phone> phones = student.getPhones();
        Phone phone1 = phones.stream().filter(phone -> phone.getNumber().equals(phoneNumber)).findFirst().orElse(null);
        if (phone1 != null) {
            phones.remove(phone1);
            student.setPhones(phones);
            studentRepository.save(student);
            System.out.println("Everything is ok");
            phone1.setStudent(null);
            phoneRepository.delete(phone1);
        }

    }

    private Student getStudentByReferance(Integer studentId) {
        Student student = studentRepository.getReferenceById(91);
        Phone phone = Phone.builder()
                .number("994106")
                .student(student)
                .build();
        phoneRepository.save(phone);

        return student;
    }

    private void addPhoneByStudentReferance(Integer studentId) {
        Student student = studentRepository.getReferenceById(91);
        Phone phone = Phone.builder()
                .number("994106")
                .student(student)
                .build();
        phoneRepository.save(phone);
    }


}
