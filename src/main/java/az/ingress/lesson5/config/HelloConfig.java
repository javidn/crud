package az.ingress.lesson5.config;

import az.ingress.lesson5.dto.Hello;
import az.ingress.lesson5.repository.BookRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class HelloConfig {

    @Bean
    @Scope("singleton")//singleton edir. Zaten default beledi, yazilmaya da biler
//    @Scope("prototype")// her Hello obyekti lazim olanda yeni obyekt yaradir.
    public Hello getHello(){
        System.out.println("Hello Bean is created.");
        return new Hello("Applee");
    }

}
