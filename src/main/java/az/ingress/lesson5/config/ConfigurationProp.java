package az.ingress.lesson5.config;


import az.ingress.lesson5.dto.User;
import az.ingress.lesson5.repository.BookRepository;
import az.ingress.lesson5.service.BookServiceImpl;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
//import az.ingress.lesson5.service.*;
import az.ingress.lesson5.service.BookService;

@Configuration
@ConfigurationProperties(prefix = "ms24")
@Data
public class ConfigurationProp {

    private  Integer lesson;
    private List<String> users;
    private List<User> objects;
    private Map<String, String> map;
//    private Integer age;




//    public ConfigurationProp(BookService bookServiceImpl){
//
//    }
}
