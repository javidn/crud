package az.ingress.lesson5.config;

import az.ingress.lesson5.model.Student2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

import java.time.Duration;

@Configuration
//@EnableRedisRepositories
public class RedisConfiguration {

    @Value("${spring.data.redis.host}")
    private String host;
    @Value("${spring.data.redis.port}")
    private Integer port;

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(host);
        configuration.setPort(port);
        return new LettuceConnectionFactory(configuration); // to make a redis bean
    }

    @Bean
    public CacheManager cacheManager() {
        RedisCacheConfiguration minute5 = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofMinutes(5)); // it means that this redis config saved for 5 minutes
        RedisCacheConfiguration day3 = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofDays(3));
        return RedisCacheManager.builder(redisConnectionFactory())
                .cacheDefaults(day3) // every cache lasts for 3 day unless student2
                .withCacheConfiguration("student2", minute5) // minute5 belongs to student2 cache
                .build();
    }

    @Bean
    public RedisTemplate<Integer, Student2> redisTemplate() {
        final RedisTemplate<Integer, Student2> template =  new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory());
        var jackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
//        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setKeySerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }
}
