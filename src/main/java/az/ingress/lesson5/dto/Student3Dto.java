package az.ingress.lesson5.dto;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student3Dto {

    Integer id;
    String firstName;
    Boolean isActive;
    Timestamp birthDay;
    Integer age;
    Integer newAge;
    String status;
}
