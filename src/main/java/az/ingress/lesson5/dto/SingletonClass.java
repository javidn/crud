package az.ingress.lesson5.dto;

public class SingletonClass {
    private static SingletonClass SINGLETON_CLASS;

    private SingletonClass(){

    }
    public static SingletonClass getSingletonClass(){
        if (SINGLETON_CLASS == null){
            synchronized (SingletonClass.class){
                if (SINGLETON_CLASS == null)
                    SINGLETON_CLASS = new SingletonClass();
            }
        }
        return SINGLETON_CLASS;
    }
}
