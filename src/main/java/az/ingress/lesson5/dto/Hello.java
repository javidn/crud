package az.ingress.lesson5.dto;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.persistence.Entity;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//@RequiredArgsConstructor
@Slf4j
public class Hello {

    private String name;

    @PostConstruct
    public void init() {
        log.info("\nHello bean intilaized.");
    }

    @PreDestroy
    public void destroy() {
        log.info("\nHello bean destroyed.\n");
    }

//    @Override
//    protected void finalize() throws Throwable {
//        System.out.println("It has worked.");
//        super.finalize();
//    }
}
