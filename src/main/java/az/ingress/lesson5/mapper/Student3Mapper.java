package az.ingress.lesson5.mapper;

import az.ingress.lesson5.dto.Student3Dto;
import az.ingress.lesson5.dto.StudentDto;
import az.ingress.lesson5.model.Student;
import az.ingress.lesson5.model.Student3;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
// if we don't write IGNORE, so, if variables' names are different, Exception will occur unless using @Mapping
public interface Student3Mapper {

    @Mapping(target = "firstName", source = "name") // if variables' names are not the same, so we will use @Mapping
     // otherwise, firstName column will set as null.
    @Mapping(target = "status", ignore = true) // Especially, status column will be ignored.
    @Mapping(constant = "true", target = "isActive") // always, active column will be set as true.
    @Mapping(target = "newAge", source = "student", qualifiedByName = "getNewAge")
    Student3Dto studentToStudentDto(Student3 student);

    @Named(value = "getNewAge")
    default Integer getNewAge(Student3 student3) {
        return student3.getAge();
    }

}
