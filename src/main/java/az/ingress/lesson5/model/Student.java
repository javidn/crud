package az.ingress.lesson5.model;

import jakarta.persistence.*;
import lombok.*;
//import org.hibernate.annotations.Cascade;
//import org.hibernate.annotations.CascadeType;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
//@NamedEntityGraph(name = "Student.authorities",
//        attributeNodes = {
//                @NamedAttributeNode("authorities"),
//                @NamedAttributeNode("phones")
//        })
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    //    @Column(name = "name")
    String name;
    @Column()
    Boolean isActive;
    Timestamp birthDay;
    Integer age;
    String status;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "adress_id", referencedColumnName = "id")
    @ToString.Exclude
//    @Cascade(CascadeType.ALL)
    Adress adress;


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "student_phone",
    joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "phone_id", referencedColumnName = "id"))
    @ToString.Exclude
    Set<Phone> phones = new HashSet<>();



//    @ManyToMany(fetch = FetchType.LAZY)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "student_authority",
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
    Set<Authority> authorities = new HashSet<>();


}
