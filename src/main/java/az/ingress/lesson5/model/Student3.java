package az.ingress.lesson5.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Student3 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    Boolean isActive;
    Timestamp birthDay;
    Integer age;
    String status;

    public Student3(Student3 student3){
        this.id = student3.getId();
        this.name = student3.getName();
        this.isActive = student3.getIsActive();
        this.birthDay = student3.getBirthDay();
        this.age = student3.getAge();
        this.status = student3.getStatus();
    }
}
