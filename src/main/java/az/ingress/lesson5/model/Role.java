package az.ingress.lesson5.model;

public enum Role {

    USER,
    ADMIN,
    THIRD
}
