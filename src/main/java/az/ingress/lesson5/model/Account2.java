package az.ingress.lesson5.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@RedisHash("account")
@Builder
public class Account2 {

    @Id
    Long id;
    Double balance;
}
