package az.ingress.lesson5.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Student2 implements Serializable {

    @Serial // yazilmaya da biler, optianal-dı
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    Boolean isActive;
    Timestamp birthDay;
    Integer age;
    String status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student2", fetch = FetchType.EAGER)
    List<Score> scores = new ArrayList<>();

    @PostLoad
    public void postLoad() {
        this.scores = new ArrayList<>(this.scores); // score-u get yaxud set edende arraylist kimi
        // edir persistcontext falan etmir // tam basa dusmesen problem deyil
    }


}
