package az.ingress.lesson5.model;

import jakarta.persistence.*;
import lombok.*;

@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Department {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;

    @ManyToOne()
    @ToString.Exclude
    @JoinColumn(name = "organization_id")
    Organization organization;
}
