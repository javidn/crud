package az.ingress.lesson5.controller;

import az.ingress.lesson5.model.Student;
import az.ingress.lesson5.service.StudentService;
import az.ingress.lesson5.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;


    @GetMapping("/get/{id}")
    public Student getStudentById(@PathVariable int id) {
        return studentService.getStudent2(id);
    }
}
