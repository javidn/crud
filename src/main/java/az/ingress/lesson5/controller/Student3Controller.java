package az.ingress.lesson5.controller;

import az.ingress.lesson5.dto.Student3Dto;
import az.ingress.lesson5.model.Student2;
import az.ingress.lesson5.service.Student2ServiceCache1AndRedisTemp;
import az.ingress.lesson5.service.Student2ServiceCache2;
import az.ingress.lesson5.service.Student3Service;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student3")
@RequiredArgsConstructor
public class Student3Controller {

    private final Student2ServiceCache1AndRedisTemp student2ServiceCache1AndRedisTemp;
    private final Student2ServiceCache2  student2ServiceCache2;
    private final Student3Service student3Service;


    @GetMapping("/{id}")
    public Student3Dto get(@PathVariable int id) {
        return student3Service.get(id);
    }


    /*
    @PutMapping
    public Student2 update(@RequestBody Student2 student2) {
//        return student2ServiceCache1AndRedisTemp.updateWithCacheManagerStudent2(student2);
        return student2ServiceCache1AndRedisTemp.updateWithRedisTemplateStudent2(student2);
//        return student2ServiceCache2.update(student2);
    }

    @PostMapping
    public Student2 create(@RequestBody Student2 student2) {
        return student2ServiceCache1AndRedisTemp.createWithRedisTemplateStudent2(student2);
//        return student2ServiceCache2.create(student2);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        student2ServiceCache1AndRedisTemp.deleteWithRedisTemplateStudent2(id);
//        student2ServiceCache2.delete(id);
    }

     */
}
