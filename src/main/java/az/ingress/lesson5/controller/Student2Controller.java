package az.ingress.lesson5.controller;

import az.ingress.lesson5.model.Student2;
import az.ingress.lesson5.service.Student2ServiceCache1AndRedisTemp;
import az.ingress.lesson5.service.Student2ServiceCache2;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student2")
@RequiredArgsConstructor
public class Student2Controller {

    private final Student2ServiceCache1AndRedisTemp student2ServiceCache1AndRedisTemp;
    private final Student2ServiceCache2  student2ServiceCache2;


    @GetMapping("/{id}")
    public Student2 get(@PathVariable int id) {
//        return student2ServiceCache1AndRedisTemp.getWithCacheManagerStudent2(id);
        return student2ServiceCache1AndRedisTemp.getWithRedisTemplateStudent2(id);
//        return student2ServiceCache2.get(id);
    }

    @PutMapping
    public Student2 update(@RequestBody Student2 student2) {
//        return student2ServiceCache1AndRedisTemp.updateWithCacheManagerStudent2(student2);
        return student2ServiceCache1AndRedisTemp.updateWithRedisTemplateStudent2(student2);
//        return student2ServiceCache2.update(student2);
    }

    @PostMapping
    public Student2 create(@RequestBody Student2 student2) {
        return student2ServiceCache1AndRedisTemp.createWithRedisTemplateStudent2(student2);
//        return student2ServiceCache2.create(student2);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        student2ServiceCache1AndRedisTemp.deleteWithRedisTemplateStudent2(id);
//        student2ServiceCache2.delete(id);
    }
}
