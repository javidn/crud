package az.ingress.lesson5.controller;

import az.ingress.lesson5.Lesson5Application;
import az.ingress.lesson5.config.ConfigurationProp;
import az.ingress.lesson5.dto.Hello;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@Slf4j
public class HelloController {


    @GetMapping
    public String get() {
        log.info("\nsent");
//        log.info("\n" + Lesson5Application.asanURL);//asanURL does not set automatically
//        new Hello();
//        System.gc();
//        System.gc();
//        System.gc();
//        Thread.sleep(3000);
        System.out.println("Last of sleepping of thread");
        System.out.println("Last of sleepping of thread");
        System.out.println("Last of sleepping of thread");
        System.out.println("Last of sleepping of thread");
//        System.gc();
        return "It is ok.";
    }

    @GetMapping("/get")
    public String hello() {
        System.out.println("Here");
        return "Hello from Hello class lesson2.But it is port 8081";
//        return "from Lesson2 v1.";
    }

}
