package az.ingress.lesson5.controller;

import az.ingress.lesson5.dto.Student3Dto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/{id}")
    public Student3Dto getStudent3Dto(@PathVariable int id) {
        return Student3Dto.builder()
                .id(id)
                .firstName("Fariz")
                .build();
    }
}
