package az.ingress.lesson5.controller;

import az.ingress.lesson5.dto.BookRequestDto;
import az.ingress.lesson5.dto.BookResponseDto;
import az.ingress.lesson5.service.BookService;
import az.ingress.lesson5.service.BookServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;


@RestController
@RequestMapping("/book")
//@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

//    public BookController(BookService bookServiceImpl2){
//        this.bookService = bookServiceImpl2;
//    }

    public BookController(@Qualifier("bookServiceImpl") BookService bookService) {
        this.bookService = bookService;
    }

//    public BookController(BookService bookService) {
//        this.bookService = bookService;
//    }

    @GetMapping("/get")
    public String get() {
        System.out.println("OKKK");
        return "Ok in BookController.";
    }

    @GetMapping("{id}")
    public ResponseEntity<BookResponseDto> get(@PathVariable() Integer id) {
        System.out.println(id);
        BookResponseDto response = bookService.get(id);
        System.out.println(response);
        return ResponseEntity.ok(response);
    }

//    @GetMapping("{id}")
//    public ResponseEntity<BookResponseDto> get2(@PathVariable Integer id) {
//        BookResponseDto response = bookService.get(id);
//        BookResponseDto responseDto = BookResponseDto.builder()
//                .name("Spiderman")
//                .author("Somebody")
//                .pageCount(555)
//                .build();
////        return ResponseEntity.ok(responseDto);
////        if (response == null)
////            return ResponseEntity.notFound().build();
//
//        System.out.println("here");
//        return ResponseEntity.ok(response);
//    }

//    @GetMapping("/get3")
//    public String get3() {
//        return "OK in get3 method.";
//    }


    @PostMapping
    public ResponseEntity<Void> create(@RequestBody BookRequestDto dto) {
        int id = bookService.create(dto);
        System.out.println("here");
        return ResponseEntity.created(URI.create("/" + id)).build();
    }

    @PostMapping("/{id}")
    public ResponseEntity<BookResponseDto> update(@PathVariable Integer id,
                                                  @RequestBody BookRequestDto dto) {
        System.out.println("id = " + id + " -> " + dto);
        BookResponseDto response = bookService.update(id, dto);
//        return ResponseEntity.ok().header("key", "value").body(response);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        bookService.delete(id);
        System.out.println("deleted");
        return ResponseEntity.ok().build();
    }
}
