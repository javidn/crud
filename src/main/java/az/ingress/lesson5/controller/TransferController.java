package az.ingress.lesson5.controller;

import az.ingress.lesson5.model.Account;
import az.ingress.lesson5.repository.AccountRepository;
import az.ingress.lesson5.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
@Slf4j
public class TransferController {

    private final TransferService transferService;
    private final AccountRepository accountRepository;

    //
    @GetMapping("/1/{id}")
//    @Transactional(readOnly = true)
    @Transactional
    public Account transfer1(@PathVariable Integer id) {
        System.out.println("IN one");
        log.info("Thread {} is started", Thread.currentThread().getName());
        Account account = accountRepository.findById(id).orElseThrow();
        log.info("Thread {} is finished", Thread.currentThread().getName());
        return account;
    }

    @GetMapping("/2/{amount}")
    public void transfer2(@PathVariable Integer amount) {
        System.out.println("IN two");
        transferService.transferWithSerializable2(1, 2, amount);
        System.out.println("finish2");
    }

    @GetMapping("/3/{amount}")
    public void transfer3(@PathVariable Integer amount) throws InterruptedException {
        System.out.println("IN three");
        log.info("Thread {} started\n", Thread.currentThread().getName());
//        transferService.transfer19(1, 2, amount);
        transferService.transfer19_3(1, 2, amount);
        System.out.println(Thread.currentThread().getName() + "aaaaaaaaaaaaaaaaaaaaaaaaa");
        System.out.println(Thread.currentThread().getName() + "aaaaaaaaaaaaaaaaaaaaaaaaa");
        System.out.println(Thread.currentThread().getName() + "aaaaaaaaaaaaaaaaaaaaaaaaa");
        System.out.println(Thread.currentThread().getName() + "aaaaaaaaaaaaaaaaaaaaaaaaa");
        log.info("Thread {} finished", Thread.currentThread().getName());
        System.out.println("finish3");
    }

    @GetMapping("/4")
    public void transfer4() throws InterruptedException {
//        log.info("Thread {} started", Thread.currentThread().getName());
//        Thread.sleep(10_000);
//        log.info("Thread {} finished", Thread.currentThread().getName());
    }

    @GetMapping("/5")
    public void transfer5() throws InterruptedException {
        System.out.println("start");
        Thread.sleep(10_000);
        System.out.println("finish");
    }
}
