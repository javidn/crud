package az.ingress.lesson5.service;

import az.ingress.lesson5.dto.StudentDto;
import az.ingress.lesson5.model.Student;
import az.ingress.lesson5.repository.StudentProjection;
import az.ingress.lesson5.repository.StudentRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepository studentRepository;
    private final EntityManagerFactory entityManagerFactory;
    private Map<Integer, Student> students = new HashMap<>();


    public Student getStudent2(Integer id) {
        if (students.containsKey(id)) {
            log.info("id: {} is found in cache", id);
            return students.get(id);
        }
        Student student = studentRepository.findById(id).orElseThrow(() -> new RuntimeException("Id is not found."));
        students.put(id, student);
        log.info("id: {} is found in DB", id);
        return student;
    }

    public Student update(Integer id, Student student) {
        studentRepository.save(student);
        students.put(id, student);
        return null;
    }

    public Student delete(Integer id) {
        studentRepository.deleteById(id);
        students.remove(id);
        return null;
    }


    public Student getStudent(Integer id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Student student = entityManager.createQuery("select s from Student s where id = :id", Student.class)
                .setParameter("id", 95)
                .getSingleResult();
        student.setAge(0);
//        entityManager.flush();
        entityManager.detach(student);
        entityManager.getTransaction().commit();
        entityManager.close();
        return student;
    }

    public Student getStudentById(int id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        Student student = entityManager.createQuery("select s from Student s where s.id = :id", Student.class)
//                .setParameter("id", id)
//                .getSingleResult();
//        entityManager.close();
//
//        Student student1 = studentRepository.findById(id).orElseThrow(() -> new RuntimeException("Student with id " + id + " not found"));

//        List<Student> allByAgeGreaterThan = studentRepository.findAllByAgeGreaterThan(22);
//        System.out.println(allByAgeGreaterThan);
//        List<Student> allByAgeLessThan = studentRepository.findAllByAgeLessThan(10);
//        System.out.println(allByAgeLessThan);
        List<StudentProjection> students = studentRepository.findallByAge(23);
//        for (StudentProjection student : students) {
//            System.out.println("Student Id: " + student.getId() + " " + " Student name: " + student.getName());
//        }
        for (StudentProjection student : students) {
            System.out.println(student.getClass().getName());
        }
//        List<Student> students1 = studentRepository.findAllByAgeAndNameLike(23, "Qurban");
//        System.out.println(students1);
//        List<Student> studentList = studentRepository.findAllByAgeAndNameLike(23, "Qurban");
//        for (Student student : studentList) {
//            System.out.println("Student name: " + student.getName());
//        }

//        List<StudentDto> studentDtos = studentRepository.findallByAgeDto(23);
//        for (StudentDto student : studentDtos) {
//            System.out.println("Student Id: " + student.getId() + " " + " Student name: " + student.getName());
//        }
//        List<Student> students2 = studentRepository.findallByAge3(23);
//        for (Student student : students2) {
//            System.out.println("Student Id: " + student.getId() + " " + " Student name: " + student.getName());
//        }
        return null;
    }


}
