package az.ingress.lesson5.service;

import az.ingress.lesson5.dto.BookRequestDto;
import az.ingress.lesson5.dto.BookResponseDto;
import az.ingress.lesson5.dto.Hello;
import az.ingress.lesson5.model.Book;
import az.ingress.lesson5.repository.BookRepository;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;
import org.hibernate.bytecode.enhance.internal.tracker.NoopCollectionTracker;
import org.springframework.stereotype.Service;

@Service
//@AllArgsConstructor
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;
    private final Hello hello;

    public BookServiceImpl(BookRepository bookRepository, Hello hello){//error yoxdu
        this.bookRepository = bookRepository;
        this.hello = hello;
    }


    @Override
    public BookResponseDto get(Integer id) {
        System.out.println("id = " + id);
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Id is not found"));

//        BookResponseDto bookResponseDto = new BookResponseDto();
//        bookResponseDto.setAuthor(book.getAuthor());
//        bookResponseDto.setId(book.getId());
//        bookResponseDto.setName(book.getName());
//        bookResponseDto.setPageCount(book.getPageCount());
//
//        if (true)
//            return bookResponseDto;

        BookResponseDto bookResponseDto1 = BookResponseDto.builder()
                .author(book.getAuthor())
                .name(book.getName())
                .id(book.getId())
                .pageCount(book.getPageCount())
                .build();
        System.out.println("bookResponseDto1 = " + bookResponseDto1);

        return bookResponseDto1;
    }

    @Override
    public int create(BookRequestDto dto) {
        Book book = Book.builder()
                .author(dto.getAuthor())
                .pageCount(dto.getPageCount())
                .name(dto.getName())
                .build();
        bookRepository.save(book);
        return book.getId();
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto dto) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("False id"));
        book.setAuthor(dto.getAuthor());
        book.setPageCount(dto.getPageCount());
        book.setName(dto.getName());
        bookRepository.save(book);
        BookResponseDto bookResponseDto = BookResponseDto.builder()
                .author(book.getAuthor())
                .name(book.getName())
                .pageCount(book.getPageCount())
                .id(book.getId())
                .build();
        return bookResponseDto;
    }

    @Override
    public void delete(Integer id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("False id"));
        bookRepository.delete(book);
    }
}
