package az.ingress.lesson5.service;

import az.ingress.lesson5.model.Account;
import az.ingress.lesson5.repository.AccountRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private static int time = 0;

    @Transactional
    public void transfer(int from, int to, int amount) throws Exception {
        Account fromAccount = accountRepository.findById(from).orElseThrow(() -> new RuntimeException("Account not found"));
        if (fromAccount.getBalance() < amount)
            throw new RuntimeException("Insufficient balance");
        Account toAccount = accountRepository.findById(to).orElseThrow(() -> new RuntimeException("Account not found"));
        fromAccount.setBalance(fromAccount.getBalance() - amount);

        accountRepository.save(fromAccount);
        if (true) {
            throw new RuntimeException("Some error");
        }
        toAccount.setBalance(toAccount.getBalance() + amount);
        accountRepository.save(toAccount);
    }

    @Transactional
    @SneakyThrows
    // It ensures If You have checked exception so You won't need to annotate with throws or to handle with a try-catch block
    public void transfer3(int from, int to, Integer amount) {
        Account fromAccount = accountRepository.findById(from).orElseThrow(() -> new RuntimeException("Account not found"));
        if (fromAccount.getBalance() < amount)
            throw new RuntimeException("Insufficient balance");
        Account toAccount = accountRepository.findById(to).orElseThrow(() -> new RuntimeException("Account not found"));
        // suppose there is very long logical here.
        Thread.sleep(10_000);
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        toAccount.setBalance(toAccount.getBalance() + amount);
    }

    public void transferSafe(int from, int to, double amount) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            Account fromAccount = accountRepository.findById(from).orElseThrow(() -> new RuntimeException("Account with " + from + " id is not found."));
            Account toAccount = accountRepository.findById(to).orElseThrow(() -> new RuntimeException("Account with " + to + " id is not found."));
            if (fromAccount.getBalance() < amount)
                throw new RuntimeException("Insufficient balance");
            fromAccount.setBalance(fromAccount.getBalance() - amount);
            toAccount.setBalance(toAccount.getBalance() + amount);
            entityManager.merge(fromAccount);
            if (true)
                throw new RuntimeException("Some error");
            entityManager.merge(toAccount);
            entityManager.getTransaction().commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @org.springframework.transaction.annotation.Transactional(rollbackFor = {AWTException.class, IOException.class})
    //IOException checked olmagina baxmayaraq rollback edecek
//    @org.springframework.transaction.annotation.Transactional
    public void transfer2(int from, int to, int amount) throws Exception {
        Account fromAccount = accountRepository.findById(from).orElseThrow(() -> new RuntimeException("Account not found"));
        if (fromAccount.getBalance() < amount)
            throw new RuntimeException("Insufficient balance");
        Account toAccount = accountRepository.findById(to).orElseThrow(() -> new RuntimeException("Account not found"));
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        accountRepository.save(fromAccount);
        if (true) {
            throw new IOException("It is an IOException");
        }
        toAccount.setBalance(toAccount.getBalance() + amount);
        accountRepository.save(toAccount);
    }


    //    @org.springframework.transaction.annotation.Transactional
    @Transactional
//    @SneakyThrows
    public void transfer19(int from, int to, int amount) throws InterruptedException {
        log.info("Thread {} - Getting account with id {}", Thread.currentThread().getName(), from);
        Account fromAccount = accountRepository.findById(from)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} - Account with id {} is {}", Thread.currentThread().getName(), from, fromAccount);
        if (fromAccount.getBalance() < amount)
            throw new RuntimeException("Insufficient balance");
        log.info("Thread {} Sleeping for 12 seconds first time", Thread.currentThread().getName());
        Thread.sleep(12_000);
        System.out.println(++time);
        if (time == 1) {
            log.info("Thread {} Sleeping for 12 seconds above additionally", Thread.currentThread().getName());
            Thread.sleep(12_000);
        }
        log.info("Thread {} - Getting account with id {}", Thread.currentThread().getName(), to);
        Account toAccount = accountRepository.findById(to)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} - Account with id {} is {}", Thread.currentThread().getName(), to, toAccount);
        log.info("Thread {} Changing balance of Account {}", Thread.currentThread().getName(), from);
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        log.info("Thread {} Changing balance of Account {}", Thread.currentThread().getName(), to);
        toAccount.setBalance(toAccount.getBalance() + amount);
        log.info("Thread {} Saving account {}", Thread.currentThread().getName(), from);
        accountRepository.save(fromAccount);
        log.info("Thread {} Saving account {}", Thread.currentThread().getName(), to);
        accountRepository.save(toAccount);
        time++;
        if (time == 3) {
            log.info("Thread {} Sleeping for 14 seconds below additionally", Thread.currentThread().getName());
            Thread.sleep(16_000);
        }
        log.info("Thread {} Sleeping for 18 seconds last time", Thread.currentThread().getName());
        Thread.sleep(18_000);
    }

    private static long threadId = -1;
    private static int order = 0;

    @Transactional
//    @SneakyThrows
    public void transfer19_2(int from, int to, int amount) throws InterruptedException {
        log.info("Thread {} - Getting account with id {}", Thread.currentThread().getName(), from);
        Account fromAccount = accountRepository.findById(from)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} - Account with id {} is {}", Thread.currentThread().getName(), from, fromAccount);
        log.info("Thread {} - Getting account with id {}", Thread.currentThread().getName(), to);
        Account toAccount = accountRepository.findById(to)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} - Account with id {} is {}", Thread.currentThread().getName(), to, toAccount);

        log.info("Thread {} Sleeping for 12 seconds first time", Thread.currentThread().getName());
        Thread.sleep(10_000);
        if (++time == 1) {
            log.info("Thread {} Changing balance of Account {}", Thread.currentThread().getName(), from);
            fromAccount.setBalance(fromAccount.getBalance() - amount);
            log.info("Thread {} Changing balance of Account {}", Thread.currentThread().getName(), to);
            toAccount.setBalance(toAccount.getBalance() + amount);
            log.info("Thread {} Saving account {}", Thread.currentThread().getName(), from);
            accountRepository.save(fromAccount);
            log.info("Thread {} Saving account {}", Thread.currentThread().getName(), to);
            accountRepository.save(toAccount);
            Thread.sleep(12_000);
        }
        if (++time == 3) {
            log.info("Thread {} - Getting account with id {}", Thread.currentThread().getName(), 3);
            Account anotherAccount = accountRepository.findById(3)
                    .orElseThrow(() -> new RuntimeException("Account not found"));
            log.info("Thread {} - Account with id {} is {}", Thread.currentThread().getName(), 3, anotherAccount);
            anotherAccount.setBalance(10_000.d);
            accountRepository.save(anotherAccount);
            System.out.println("Saving 10000");
//            Thread.sleep(8_000);
        }
        Thread.sleep(8_000);
        log.info("Thread {} finished in service", Thread.currentThread().getName());

    }

    @Transactional
//    @SneakyThrows
    public void transfer19_3(int from, int to, int amount) throws InterruptedException {
        if(threadId == -1){
            threadId = Thread.currentThread().getId();
            log.info("Thread's {} with id {} is assigned to variable.", Thread.currentThread().getName(),
                    Thread.currentThread().getId());
        }

        log.info("Thread {} - Getting account with id {}", Thread.currentThread().getName(), from);
        Account fromAccount = accountRepository.findById(from)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} - Account with id {} is {}", Thread.currentThread().getName(), from, fromAccount);
        log.info("Thread {} - Getting account with id {}", Thread.currentThread().getName(), to);
        Account toAccount = accountRepository.findById(to)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} - Account with id {} is {}", Thread.currentThread().getName(), to, toAccount);


        if (Thread.currentThread().getId() != threadId) {
            log.info("Thread {} Sleeping for 10 seconds first time", Thread.currentThread().getName());
            Thread.sleep(10_000);
        }

        System.out.println(Thread.currentThread().getName() + " 1----------- ");

            log.info("Thread {} Changing balance of Account {}", Thread.currentThread().getName(), from);
            fromAccount.setBalance(fromAccount.getBalance() - amount);
            log.info("Thread {} Changing balance of Account {}", Thread.currentThread().getName(), to);
            toAccount.setBalance(toAccount.getBalance() + amount);
            log.info("Thread {} Saving account {}", Thread.currentThread().getName(), from);
            accountRepository.save(fromAccount);
            log.info("Thread {} Saving account {}", Thread.currentThread().getName(), to);
            accountRepository.save(toAccount);
        System.out.println(Thread.currentThread().getName() + " 2----------- ");


        log.info("Thread {} Sleeping for 8 seconds in the end.", Thread.currentThread().getName());
        Thread.sleep(8_000);
        log.info("Thread {} finished in service", Thread.currentThread().getName());

    }


    // Isolation levels

    // READ_UNCOMITTED is not supported by postgres
    // READ_COMMITTED +
    // REPEATABLE_READ +
    // SERIALIZABLE -

    @org.springframework.transaction.annotation.Transactional(isolation = Isolation.SERIALIZABLE)
    @SneakyThrows
    // It ensures If You have checked exception so You won't need to annotate with throws or to handle with a try-catch block
    public void transferWithSerializable1(int from, int to, Integer amount) {
        Thread.sleep(3000);
//        List<Account> all = accountRepository.findAll();
//        System.out.println(all);
        Account fromAccount = accountRepository.findById(from).orElseThrow(() -> new RuntimeException("Account not found"));
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        Thread.sleep(16_000);
    }

    @org.springframework.transaction.annotation.Transactional(isolation = Isolation.SERIALIZABLE)
    @SneakyThrows
    // It ensures If You have checked exception so You won't need to annotate with throws or to handle with a try-catch block
    public void transferWithSerializable2(int from, int to, Integer amount) {
        Thread.sleep(6_000);
        Account account = accountRepository.findById(from).get();
        System.out.println(account.getBalance() + " first step");
        Thread.sleep(2000);
        Account account1 = accountRepository.findById(from).get();
        System.out.println(account.getBalance() + " second step");

        System.out.println(account);
//        account.setBalance(account.getBalance() - amount);
//        accountRepository.save(account);
        System.out.println("Account1 balance: " + account1.getBalance());
    }

}
