package az.ingress.lesson5.service;


import az.ingress.lesson5.dto.BookRequestDto;
import az.ingress.lesson5.dto.BookResponseDto;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


public interface BookService {

    BookResponseDto get(Integer id);
    int create(BookRequestDto dto);
    BookResponseDto update(Integer id, BookRequestDto dto);
    void delete(Integer id);

}
