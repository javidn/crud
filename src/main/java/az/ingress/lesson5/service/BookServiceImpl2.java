package az.ingress.lesson5.service;

import az.ingress.lesson5.dto.BookRequestDto;
import az.ingress.lesson5.dto.BookResponseDto;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
//@Primary
public class BookServiceImpl2 implements BookService{
    @Override
    public BookResponseDto get(Integer id) {
        return null;
    }

    @Override
    public int create(BookRequestDto dto) {
        return 0;
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto dto) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }
}
