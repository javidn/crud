package az.ingress.lesson5.service;

import az.ingress.lesson5.model.Student2;
import az.ingress.lesson5.repository.Student2Repository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class Student2ServiceCache2 {

    private final Student2Repository student2Repository;


    @Transactional
    @Cacheable(value = "student", key = "#id") // It checks cache, firstly. If there is no, then The method's body processes and fetches from db
    public Student2 get(Integer id) {
        Student2 student2 = student2Repository.findById(id).orElseThrow();
        System.out.println("From db");
        return student2;
    }

    @Transactional
    @CachePut(value = "student", key = "#result.id")
    public Student2 create(Student2 student2) {
        return student2Repository.save(student2);
    }


    @Transactional
    @CachePut(value = "student", key = "#result.id")
    public Student2 update(Student2 student2) {
        Student2 student = student2Repository.save(student2);
        return student;
    }


    @Transactional
    @CacheEvict(value = "student", key = "#id")
    public void delete(int id) {
        student2Repository.deleteById(id);
    }


}
