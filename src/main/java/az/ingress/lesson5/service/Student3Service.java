package az.ingress.lesson5.service;

import az.ingress.lesson5.dto.Student3Dto;
import az.ingress.lesson5.mapper.Student3Mapper;
import az.ingress.lesson5.model.Student3;
import az.ingress.lesson5.repository.Student2Repository;
import az.ingress.lesson5.repository.Student3Repository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class Student3Service {

    private final Student2Repository student2Repository;
    private final Student3Repository student3Repository;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Student3Mapper student3Mapper;


    @Transactional
    public Student3Dto get(Integer id) {
        Student3 student3 = student3Repository
                .findById(id).orElseThrow(() -> new IllegalArgumentException(String.format("Student3 with id %s not found", id)));
//        Student3Dto student3Dto = objectMapper.convertValue(student3, Student3Dto.class);
        Student3Dto student3Dto = student3Mapper.studentToStudentDto(student3);
        return student3Dto;
    }

    @Transactional
    public Student3 create(Student3 student3) {
        return student3Repository.save(student3);
    }


    @Transactional
    public Student3 update(Student3 student3) {
        Student3 student = student3Repository.save(student3);
        return student;
    }


//    @Transactional // The test is for the case which does not have @Transactional annotation.
    // If You want to test the case which has annotation, so it is enough to test If result and your first
    // object is equal. Because case with transaction saves only one time while the transaction does "roll back".
    // It means that you don't need to capture the save method.
    public Student3 updateName(int id) { // it is for unit testing
        Student3 student = student3Repository.findById(id).get();
//        String oldName = student.getName();
        student.setName("NewName");
        student3Repository.save(student);
//        student.setName(oldName);
        return student;
    }


    @Transactional
    public void delete(int id) {
        student3Repository.deleteById(id);
    }


}
