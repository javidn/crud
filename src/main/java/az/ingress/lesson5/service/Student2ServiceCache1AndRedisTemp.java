package az.ingress.lesson5.service;

import az.ingress.lesson5.model.Student2;
import az.ingress.lesson5.repository.Student2Repository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class Student2ServiceCache1AndRedisTemp {

    private final Student2Repository student2Repository;
    private final RedisTemplate<Integer, Student2> redisTemplate;

    private final CacheManager cacheManager;

    @Transactional
    public Student2 createWithCacheManagerStudent2(Student2 student2) {
        Student2 student3 = student2Repository.save(student2);
        Cache cache = cacheManager.getCache("student2");
        cache.put(student2.getId(), student2);
        return student3;
    }

    @Transactional
    public Student2 createWithRedisTemplateStudent2(Student2 student2) {
        Student2 student23 = student2Repository.save(student2);
        redisTemplate.opsForValue().set(student2.getId(), student2);
        return student23;
    }

    @Transactional
    public Student2 getWithCacheManagerStudent2(int id) {
        Cache cache = cacheManager.getCache("student2");
        Student2 student2 = cache.get(id, Student2.class);
        if (student2 == null) {
            Student2 student3 = student2Repository.findById(id).orElseThrow();
            System.out.println("From db");
            cache.put(student3.getId(), student3);
            return student3;
        }
        System.out.println("From cache");
        return student2;
    }

    @Transactional
    public Student2 getWithRedisTemplateStudent2(int id) {
        Student2 student2 = redisTemplate.opsForValue().get(id);
        if (student2 == null){
            Student2 student3 = student2Repository.findById(id).orElseThrow();
            System.out.println("From db");
            redisTemplate.opsForValue().set(student3.getId(), student3);
            return student3;
        }
        System.out.println("From redis");
        return student2;
    }

    @Transactional
    public Student2 updateWithCacheManagerStudent2(Student2 student2) {
        Student2 student = student2Repository.save(student2);
        Cache cache = cacheManager.getCache("student2");
        cache.put(student2.getId(), student2);
        return student;
    }

    @Transactional
    public Student2 updateWithRedisTemplateStudent2(Student2 student2) {
        Student2 student21 = student2Repository.save(student2);
        redisTemplate.opsForValue().set(student2.getId(), student2);
        return student21;
    }

    @Transactional
    public void deleteWithCacheManagerStudent2(int id) {
        student2Repository.deleteById(id);
        Cache cache = cacheManager.getCache("student2");
        cache.evict("student2");
    }

    @Transactional
    public void deleteWithRedisTemplateStudent2(int id) {
        student2Repository.deleteById(id);
        redisTemplate.opsForValue().getAndDelete(id); // it ensures that value will be gotten and key
        // will be deleted. And it ensures that The value will not be changed between retrieval and deletion.
        redisTemplate.delete(id); // It ensures the key is removed from redis without  interacting with the
        // value itself.
    }

}
