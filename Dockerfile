FROM openjdk:17-alpine
# yeni ki icinde jdk 17 olan alpine imagi var. Onu yukle
COPY build/libs/*.jar /app/
# Yeni ki, build/libs icindeki sonu .jar olan fayllari
# alpine-in icindeki app folderinin icine at

WORKDIR /app/
# Yeni app folderinin icine get

RUN mv /app/*.jar /app/app.jar
# Yeni ki *.jar-i app.jar et

ENTRYPOINT ["java"]
#yeni ki butun commandlar 'java' ile baslayacaq
#amma bu zaman CMD-nin icinde "java" yazmaqga ehtiyac yoxdu
#cunki etrypint-de "java" verilib artiq
# bu cur ->  CMD ["-jar", "app.jar"]

CMD ["java", "-jar", "app.jar"]
# app.jar faylini run etmek

#FROM alpine:3.11.2
# eger yukleyeceyimiz image-in icinde jdk yoxdursa kodda oldugu kimi yuxarida image-i
# asagida ise jdk-ni yukleyirik
#RUN apk add --no-cache openjdk11